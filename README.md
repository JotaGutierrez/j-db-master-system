Web page for simple mysql database management. 

It  allows you to make one or several queries and shows results or errors. 
I use it as an easy and fast way to test and debug queries without the need for some more complex software like phpmyadmin. 
It also dumps table definitions and contents so you can dump them in another database.

Just define server host, user, pass and database name and it is ready!

You can even execute php code, so use it with caution!!

AND DON'T USE THIS ON PRODUCTION!!!