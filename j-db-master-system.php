<?php
/**
 * @Author: fry
 * @Date:   2016-03-15 15:27:10
 * @Last Modified by:   Jose Elias Gutierrez
 * @Last Modified time: 2016-03-15 15:47:58
 */

error_reporting(E_ALL);
ini_set('display_errors', false);

define('HOST',          '');
define('DB_USER',       '');
define('DB_PASS',       '');
define('DB_NAME',       '');

$mysqli = new mysqli(HOST, DB_USER, DB_PASS, DB_NAME);

function dumpDatabaseContents(){
    $sql = "SHOW TABLES FROM " . DB_NAME;
    $result = $mysqli->query($sql);
    $return = '<pre>';
    while ($tablename = $result->fetch_row()) {
        
        $table = $tablename[0];
        $result2 = $mysqli->query('SELECT * FROM '.$table);
        $field_count = $result2->field_count;
        
        //  DUMP TABLE DEFINITIONS
        //$return.= 'DROP TABLE '.$table.';';
        $row2 = $mysqli->query('SHOW CREATE TABLE '.$table)->fetch_row();
        $return.= "\n\n".$row2[1].";\n\n";
        
        $return.="\n\n\n";
    }
    $return .= '</pre>';
    echo $return;
}

function dumpTableContents($table){
    $result2 = $mysqli->query('SELECT * FROM '.$table);
    $field_count = $result2->field_count;
    
    for ($i = 0; $i < $field_count; $i++) 
    {
        while($row = $result2->fetch_row())
        {
            $return.= 'INSERT INTO '.$table.' VALUES(';
            for($j=0; $j<$field_count; $j++) 
            {
                $row[$j] = addslashes($row[$j]);
                $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                if (isset($row[$j])) { $return.= '"'.htmlspecialchars($row[$j]).'"' ; } else { $return.= '""'; }
                if ($j<($field_count-1)) { $return.= ','; }
            }
            $return.= ");\n";
        }
    }
    
    $return.="\n\n\n";
    echo $return;
}

if(isset($_POST['query'])||isset($_POST['multiquery'])){
    if(isset($_POST['action'])){
        if($_POST['action'] == 'dump'){
            ob_start();
            if($_POST['query'] != '')
                dumpTableContents($_POST['query']);
            else
                dumpDatabaseContents();
            ob_flush();
        }else{
            ob_start();
            $result = eval($_POST['query']);
            if($result !== false)
                ob_flush();
            else
                echo error_get_last();
        }
    }else{
        $query = (isset($_POST['query']) ? $_POST['query'] : $_POST['multiquery'] );

        $num_rows = 0;
        $affected_rows = 0;
        $field_count = 0;

        if(isset($_POST['multiquery'])){
            if ($mysqli->multi_query($query)) {
                do {
                    if ($result = $mysqli->store_result()) {
                        $num_rows += $result->num_rows;   
                        $field_count += $result->field_count;
                        $result->free();
                    }
                } while ($mysqli->next_result());
                $affected_rows = $mysqli->affected_rows;   
            }
        }else{
            $result = $mysqli->query($query);
            $num_rows = $result->num_rows;   
            $affected_rows = $mysqli->affected_rows;   
            $field_count = $result->field_count;
        }

        echo '<div class="num_rows">Num rows: ' . $num_rows . '</div>';
        echo '<div class="num_rows">Affected rows: ' . $affected_rows .'</div>';
        echo '<div class="num_rows">Num fields: ' . $field_count . '</div>';
        echo '<div class="result">';
        echo '<table class="table table-condensed table-striped"><tr>';
        if($result){
            $columns = $result->field_count; 
            $fieldNames = array();
            for($i = 0; $i < $columns; $i++) { 
                $fieldName = mysqli_fetch_field_direct($result,$i)->name;
                $fieldNames[] = $fieldName;
                echo '<th>'.$fieldName .'</th>';
            }
            echo '</tr>';
            while ($row = mysqli_fetch_array($result)) {
                echo '<tr>';
                foreach ($fieldNames as $key => $value) {
                    echo "<td><div class='td_content'>".$row[$value].'</div></td>';
                }

                echo '</tr>';
            }
        }
        echo '</table><div class="error">error: ' . $mysqli->error;
        echo '</div>';
    }
    exit();
}
?>
<html>

    <header>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

        <style>
            body{
                margin: 0;
                padding: 0;
            }

            #historial {
                border: 1px solid #EEE;
                padding:10px;
            }

            #historial li{
                border-bottom:1px solid #FFF; 
            }

            #historial li:hover{
                background: #EEE;
                cursor: pointer;
                border-bottom:1px solid #CCC; 
            }

            .btn-action{
                background: #EEF;
                cursor: pointer;
                border:1px solid #CCC;    
                border-radius: 4px;
                width:120px;
                margin:10px;
                line-height: 24px;
                text-align: center;
            }

            #query {
                width: 100%;
                height: 150px;
            }

            .num_rows{
                padding-left: 4px;
                margin: 10px;
                background: #EEF;
                border:1px solid #CCC;    
                border-radius: 4px;
            }
            .error{
                margin: 10px;
                background: #FEE;
                border:1px solid #CCC;    
                border-radius: 4px;
                text-align: center;
            }

            .result {
                margin: 10px;
                width: 100%;
                overflow-x: scroll; 
            }

            .header{
                font-size: 24px;
                font-weight: light;
                text-align: center;
                background: #EEE;
                border-bottom: 1px solid #CCC;
                margin-bottom: 12px;
                padding-bottom: 12px;
                font-family: Verdana;
            }

            .container{
                margin:18px auto;
            }

            table tr > td > div.td_content{
                max-height:48px;
                overflow:hidden;
            }

        </style>
    </header>
    <body>
        <div class='header'>
            SQL Master System
        </div>

        <div class='header'>
            <?
            
            $sql = "SHOW TABLES FROM " . DB_NAME;
            $result = $mysqli->query($sql);
            echo "<ul class='unstyled inline'>";
            while ($row = $result->fetch_row()) {
                echo "<li class='btn select_table' data-table='{$row[0]}'>{$row[0]}</li>";
            }
            echo "</ul>";

            ?>
        </div>


        <div class="container">
            <textarea id="query" rows="4" cols="50">
            </textarea>
            <ul class="list-inline list-unstyled">
                <li><div id='send' class="btn-action">Query!!</div></li>
                <li><div id='sendmulti' class="btn-action">MultiQ!!</div></li>
                <li><div id='execute' class="btn-action">Execute!!</div></li>
                <li><div id='dump' class="btn-action">dump content!!</div></li>
                <li><div id='dump-table' class="btn-action">dump table!!</div></li>
            </ul>
            <p>Historial:</p>
            <ul id='historial' style='max-height:250px; overflow:scroll; list-style-type:none;'>
               
            </ul>
            <p>Respuesta: <button onclick="copyToClipboard(document.getElementById('result').innerHTML)">Copy text</button></p>
            <script>
              function copyToClipboard(text) {
                window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
              }
            </script>
            <div id='result'>

            </div>
        </div>

        <script type='text/javascript'>
        $(document).ready(function(){

            $('body').on('click', '#historial li', function(){
                $('#query').val($(this).find('.query').html());
            });

            $('body').on('click', '#historial li .exec', function(){
                $('#query').val($(this).find('.query').html());
                $('#send').trigger('click');
            });

            $('#send').on('click', function(){
                if($('#query').val()!= ''){
                    $('#historial').append('<li><span class="exec pointer" data-query="'+$('#query').val()+'">Ejecutar</span>  <span class="query">'+$('#query').val()+'</span></li>');
                    console.log($('#query').val());
                    $.post("query.php", {'query': $('#query').val()}).done(function(data){
                        $('#result').html(data);
                    });
                }
            });

            $('#sendmulti').on('click', function(){
                if($('#query').val()!= ''){
                    $('#historial').append('<li><span class="exec pointer" data-query="'+$('#query').val()+'">Ejecutar</span>  <span class="query">'+$('#query').val()+'</span></li>');
                    $.post("query.php", {'multiquery': $('#query').val()}).done(function(data){
                        $('#result').html(data);
                    });
                }
            });

            $('#dump').on('click', function(){
                $.post("query.php", {'action': 'dump', 'query': ''}).done(function(data){
                    $('#result').html(data);
                });
            });

            $('#dump-table').on('click', function(){
                $.post("query.php", {'action': 'dump', 'query': $('#query').val()}).done(function(data){
                    $('#result').html(data);
                });
            });


            $('#execute').on('click', function(){
                if($('#query').val()!= ''){
                    $('#historial').append('<li><span class="exec pointer" data-query="'+$('#query').val()+'">Ejecutar</span>  <span class="query">'+$('#query').val()+'</span></li>');
                    $.post("query.php", {'action': 'exec', 'query': $('#query').val()}).done(function(data){
                        $('#result').html(data);
                    });
                }
            });

            $('.select_table').on('click', function(){
                $('#query').val('SELECT * FROM ' + $(this).attr('data-table'));
                $('#send').trigger('click');
            });

        });
        </script>
    </body>
</html>